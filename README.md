## Instalacion

Seguir los siguientes paso para la instalacion en windows.

1. Descargar e instalar **XAMPP** que contenga version de php 7.X .
2. Descargar e instalar **POSTGRES11**
3. Buscar el archivo **PHP.INI** y descomentar las siguientes dos lineas *;extension=pdo_pgsql.so* , *;extension=pgsql.so*.
4. Ir al panel de confuguracion de  **XAMPP** y reiniciar los servicios de apache.
5. Crear una base de datos dentro del **POSTGRES** para utilizarla
6. Clonar el repositorio de la api *git clone https://bitbucket.org/ovi1008/oviapi.git*
7. Ingresar al archivo **.env** y cambiar los valores de *DB_DATABASE=?* , *DB_USERNAME=?* , *DB_PASSWORD=?*
8. Dentro de la carpeta raiz del repositorio hacer en consola **php artisan config:cache**
9. Ahora se corren los datos de prueba con **php artisan migrate --seed**
10. Listo para utilizar correlo con **php artisan serve** y ingresar a la url proporcionada.

---

## Consejos en entornos Linux

Si lo piensas implementar en un linux, pongo algunos consejos.

1. Instalar **php 7.X** con los siguientes comandos:
    - *sudo apt-get update*
    - *sudo apt -y install software-properties-common*
    - *sudo add-apt-repository ppa:ondrej/php*
    - *sudo apt-get update*
    - *sudo apt -y install php7.4*
2. Si hay problemas con la instalacion de **Postgrest11** *[guiaPostgres11](https://computingforgeeks.com/install-postgresql-11-on-ubuntu-linux/)*.
3. Instalar **apache** con el siguiente comando *sudo apt-get install apache2 libapache2-mod-php7.x*
4. El **paso3** se puede sustituir por este comando *apt-get install php-pgsql*

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---


**LARAVEL API REST CREADA POR JORGE OVIDIO MEJIA ALVARADO**

Contiene lo siguiente:
 - Validacion por **JWT**
 - Proceso de **login**
 - CRUD de **usuarios**
 - CRUD de **productos**
 - Envio de correo para recuperacion de contraseña

*Aqui se encentra el [repositorio](https://bitbucket.org/ovi1008/oviapi/src/master/) *

---

## Nota

Dentro del repositorio estan agregado lo que es una carpeta llamada **DatosImportantes** que contiene:
 - Scripts de la base de datos
 - Url de los **endPoint** que maneja la api
 - Coleccion de **POSTMAN** de los endpoints
 - Documentacion de la coleccion de **POSTMAN**