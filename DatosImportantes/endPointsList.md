# Lista de EndPoints de la api

La url que se pondra de prueba es la del sitio donde esta desplegada la api que es : **http://54.91.31.158**, esa si esta desplegada en un ambiente local se sustituye por la siguiente **http://127.0.0.1:8000**

Tods los endpoinst debe agregarse lo siguientes **headers**:
- "Content-Type": "application/json"

- Este **header** solo se exceptua en el */login* y */registerUser* y los demas simepre va ques : "Authorization": "Bearer eyJ0eXAiOiJ"

## Modulo de autenticacion

- http://54.91.31.158/api/auth/login
    - "email" : "jorge.ovi10@gmail.com"
    - "password" : "admin123"
    - Es metodo **POST**

- http://54.91.31.158/api/auth/logout
    - "token": "eyJpc3MiOiJodHRwOlwvXC81NC45MS4zMS4xNThcL2FwaVwvYXV0a"
    - Es metodo **POST**
    
- http://54.91.31.158/api/auth/refresh
    - Es metodo **POST**

- http://54.91.31.158/api/auth/registerUser
    - "name": "Jorge Mejia"
    - "user_name": "jorOvi191919"
    - "phone": "777777"
    - "birth_date": "31/10/1994"
    - "password": "admin1234"
    - "email": "jorge.ovi10@gmail.com"
    - Es metodo **POST**


## Modulo de usuario

- http://54.91.31.158/api/user/add
    - "name": "Jorge Mejia"
    - "user_name": "jorOvi191919"
    - "phone": "777777"
    - "birth_date": "31/10/1994"
    - "password": "admin1234"
    - "email": "jorge.ovi10@gmail.com"
    - Es metodo **POST**

- http://54.91.31.158/api/user/update/{id}
    - {id} = 1 **Parametro de la url**
    - "name": "Jorge Mejia"
    - "user_name": "jorOvi191919"
    - "phone": "777777"
    - "birth_date": "31/10/1994"
    - "password": "admin1234"
    - "email": "jorge.ovi10@gmail.com"
    - "_method": "PUT"   **solamente en peticion post en postman**
    - Es metodo **PUT** en el sistema, desde *postman* se envia atraves de un **POST** especificandole que es put con *_method = PUT*

- http://54.91.31.158/api/user/show/{id}
    - {id} = 1 **Parametro de la url**
    - Es metodo **GET**

- http://54.91.31.158/api/user/all?page=1
    - Es metodo **GET**

- http://54.91.31.158/api/user/delete/{id}
    - {id} = 1 **Parametro de la url**
    - Es metodo **DELETE**



## Modulo de producto

- http://54.91.31.158/api/products/add
    - "sku": "sd45645"
    - "nombre": "coca cola"
    - "cantidad": "22"
    - "precio": "2.46"
    - "descripcion": "Bebida carbonatada"
    - "imagen": "file.png"
    - Es metodo **POST**

- http://54.91.31.158/api/products/update/{id}
    - {id} = 1 **Parametro de la url**
    - "sku": "sd45645"
    - "nombre": "coca cola"
    - "cantidad": "22"
    - "precio": "2.46"
    - "descripcion": "Bebida carbonatada"
    - "imagen": "file.png"
    - "_method": "PUT"   **solamente en peticion post en postman**
    - Es metodo **PUT** en el sistema, desde *postman* se envia atraves de un **POST** especificandole que es put con *_method = PUT*

- http://54.91.31.158/api/products/show/{id}
    - {id} = 1 **Parametro de la url**
    - Es metodo **GET**

- http://54.91.31.158/api/products/all?page=1
    - Es metodo **GET**

- http://54.91.31.158/api/products/delete/{id}
    - {id} = 1 **Parametro de la url**
    - Es metodo **DELETE**
    
- http://54.91.31.158/api/products/search/{value}?page=1
    - {value} = coca **Parametro de la url**
    - Es metodo **GET**


## Envio de correo

Solamente lleva **header** de "Content-Type": "application/json"

- http://54.91.31.158/api/password/email
    - "email": "jorge.ovi10@gmail.com"
    - Es metodo **POST**

    



