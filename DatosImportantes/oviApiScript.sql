--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.7

-- Started on 2020-08-27 04:55:14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 26465)
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 26463)
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- TOC entry 2864 (class 0 OID 0)
-- Dependencies: 201
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- TOC entry 197 (class 1259 OID 26435)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 26433)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2865 (class 0 OID 0)
-- Dependencies: 196
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 200 (class 1259 OID 26456)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 26477)
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id bigint NOT NULL,
    sku character varying(12),
    nombre character varying(255) NOT NULL,
    cantidad integer NOT NULL,
    precio double precision NOT NULL,
    descripcion character varying(255),
    imagen character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.products OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 26475)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- TOC entry 2866 (class 0 OID 0)
-- Dependencies: 203
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- TOC entry 199 (class 1259 OID 26443)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    user_name character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    birth_date character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 26441)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2867 (class 0 OID 0)
-- Dependencies: 198
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2713 (class 2604 OID 26468)
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- TOC entry 2711 (class 2604 OID 26438)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2715 (class 2604 OID 26480)
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- TOC entry 2712 (class 2604 OID 26446)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2856 (class 0 OID 26465)
-- Dependencies: 202
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- TOC entry 2851 (class 0 OID 26435)
-- Dependencies: 197
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2020_08_26_061608_create_products_table	2
\.


--
-- TOC entry 2854 (class 0 OID 26456)
-- Dependencies: 200
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
jorge.ovi10@gmail.com	$2y$10$nbMybwy3swMl9pvEhFfxwOEOSrM.e9ryPZOvb85KpruLC9tLG/3XO	2020-08-27 04:53:52
\.


--
-- TOC entry 2858 (class 0 OID 26477)
-- Dependencies: 204
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, sku, nombre, cantidad, precio, descripcion, imagen, created_at, updated_at) FROM stdin;
10	CONSCC1125	COCA COLA	22	1.55000000000000004	Bebida energetica carbonatada	mW01MwHGWoZfDjI4at1E26Co9jVR008UFlebZSSs.jpeg	2020-08-26 07:41:09	2020-08-27 02:01:41
12	\N	PEPSI	2	1.64999999999999991	\N	\N	2020-08-26 07:44:24	2020-08-26 07:44:24
24	\N	FRESCA	5	2.87000000000000011	\N	3ILbQE9htQXqijXZqbfAfJJrpiZsctHCoxEuNBEM.jpeg	2020-08-26 08:57:36	2020-08-26 08:57:36
25	\N	UVA	9	2.5	Bebida sabor a uva	tjxZOYlBspoX62Sa4eKMv8oRaUHH5lCb1QTCGM2t.jpeg	2020-08-26 09:00:23	2020-08-26 09:00:23
27	\N	Croquetas Toffee	3	3.75999999999999979	\N	\N	2020-08-27 08:36:20	2020-08-27 08:36:20
\.


--
-- TOC entry 2853 (class 0 OID 26443)
-- Dependencies: 199
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, user_name, phone, birth_date, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
1	Jorge Mejia	jorge.ovi10@gmail.com	ovi10	77777777	17-05-1994	\N	$2y$10$4Jj.Ry/b0ZYF7e6iNibV2.Os3hc0TftY26/70xo21LFsd9xY2yA2S	\N	2020-08-25 06:48:46	2020-08-25 06:48:46
5	Jorge Mejia Alvardo	jorge.ovi10+1@gmail.com	ovi08	4444444	31/10/1994	\N	$2y$10$zn2aZbG6ioMyHbtyhdcAIOKiKIxEYkeQHOoJbXWMgGKmDKEwvhf5.	\N	2020-08-25 08:53:54	2020-08-25 08:53:54
6	Ovidio	jorge.ovi10+2@gmail.com	jorge08	4445757	01/12/1996	\N	$2y$10$LJn0.gmOvLCdlCtdCplgYelpnk/31jTpxftwD/HEsPwhSJ4KoNRk2	\N	2020-08-26 04:21:11	2020-08-26 05:23:57
7	Jorge Mejia	jorge.ovi10+3@gmail.com	ovi103	4444444	31/10/1994	\N	$2y$10$ouic/OT2yB9HWkGeN9hLluEQNRywJ130lvVCmjbA7JhfOSdxk5b22	\N	2020-08-26 05:26:34	2020-08-26 05:26:34
8	Jorge Mejia	jorge.ovi10+4@gmail.com	ovi104	4444444	31/10/1994	\N	$2y$10$bFeoB4VtZAwShTyqP3kJDeA1e58RoZZa0mIp8TQHZh7bzieuiVceO	\N	2020-08-26 05:26:43	2020-08-26 05:26:43
9	Jorge Mejia	jorge.ovi10+5@gmail.com	ovi105	4444444	31/10/1994	\N	$2y$10$7oJfFfZy.vnlSWf3eooBneueeLOYPCYIB/nLbLWNyXy/a/Uc96lMC	\N	2020-08-26 05:27:15	2020-08-26 05:27:15
\.


--
-- TOC entry 2868 (class 0 OID 0)
-- Dependencies: 201
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- TOC entry 2869 (class 0 OID 0)
-- Dependencies: 196
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 4, true);


--
-- TOC entry 2870 (class 0 OID 0)
-- Dependencies: 203
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 27, true);


--
-- TOC entry 2871 (class 0 OID 0)
-- Dependencies: 198
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 10, true);


--
-- TOC entry 2726 (class 2606 OID 26474)
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- TOC entry 2717 (class 2606 OID 26440)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2728 (class 2606 OID 26485)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- TOC entry 2719 (class 2606 OID 26453)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2721 (class 2606 OID 26451)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2723 (class 2606 OID 26455)
-- Name: users users_user_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_user_name_unique UNIQUE (user_name);


--
-- TOC entry 2724 (class 1259 OID 26462)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


-- Completed on 2020-08-27 04:55:14

--
-- PostgreSQL database dump complete
--

