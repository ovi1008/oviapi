<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class UserController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function addUser(Request $request){        
        try{

            $rules =
            [
                'name' => ['required','string', 'max:255', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ]*)*)+$/'],
                'user_name' => ['required','string', 'max:255', 'unique:users'],
                'phone' => ['required','string', 'max:255', 'regex:/^[0-9]*$/'],
                'birth_date' => ['required','string', 'max:255', 'regex:/^([0-2][1-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))(\/)\d{4}$/'],
                'password' => ['required', 'string', 'min:8'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ];
            $validator = Validator::make($request->all(), $rules);
            $errors = $validator->errors();
            //error_log(count($errors));            
            if(count($errors) == 0){
                DB::beginTransaction();
                $user = new User();
                $user->name = $request->name;
                $user->user_name = $request->user_name;
                $user->phone = $request->phone;
                $user->birth_date = $request->birth_date;
                $user->password = bcrypt($request->password);
                $user->email = $request->email;
                $user->save();
                DB::commit();
                return response()->json(['mensaje' => 'Se agrego con exito el nuevo usuario', $user],201);
            }else{
                return response()->json(['error' => $errors, 'mensaje' => 'No se pudo ingresar debido a errores en campos'], 400);
            }
        } catch(\Exception $e){
            DB::rollback();
            return response()->json(['mensaje' => 'Hubo error dentro del sistema, vuelve a intentarlo'],400);
        }              

    }

    public function updateUser($id, Request $request){         
        $rul =
        [
            'id' => ['required', 'regex:/^[0-9]*$/'],
        ];
        $message = [
            'regex' => 'La clave que se esta enviado para editar y buscar al usuario es invalida',
        ];
        $validator = Validator::make(array('id' => $id), $rul, $message);
        $errorId = $validator->errors();
        if( count($errorId) == 0 ){
            $user = User::find($id);
            if($user == null){
                return response()->json(['mensaje' => 'No se encontro ningun usuario para actualizar con esa clave'], 404);
            }
        }else{
            return response()->json(['error' => $errorId, 'mensaje' => 'Error en clave primaria'], 400);
        }                
        try{
            $rules =
            [
                'name' => ['required','string', 'max:255', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ]*)*)+$/'],
                'user_name' => 'required','string', 'max:255', 'unique:users,user_name'.(int)$id,
                'phone' => ['required','string', 'max:255', 'regex:/^[0-9]*$/'],
                'birth_date' => ['required','string', 'max:255', 'regex:/^([0-2][1-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))(\/)\d{4}$/'],
                'password' => ['required', 'string', 'min:8'],
                'email' => 'required', 'string', 'email', 'max:255', 'unique:users,email'.(int)$id,
            ];
            $validator = Validator::make($request->all(), $rules);
            $errors = $validator->errors();           
            if(count($errors) == 0){
                DB::beginTransaction();                
                $user->name = $request->name;
                $user->user_name = $request->user_name;
                $user->phone = $request->phone;
                $user->birth_date = $request->birth_date;
                $user->password = bcrypt($request->password);
                $user->email = $request->email;
                $user->save();
                DB::commit();
                return response()->json(['mensaje' => 'Se edito con exito el usuario', $user],200);
            }else{
                return response()->json(['error' => $errors, 'mensaje' => 'No se pudo ingresar debido a errores en campos'], 400);
            }
        } catch(\Exception $e){
            DB::rollback();
            return response()->json(['mensaje' => 'Hubo error dentro del sistema, vuelve a intentarlo'],400);
        }              

    }

    public function getAllUser(){
        $users = User::paginate(3);
        if($users == null){
            return response()->json(['mensaje' => 'No se encuentra ningun usuario'], 404);
        }else{
            return response()->json($users, 200);
        }
    }

    public function getUser($id){
        $rul =
        [
            'id' => ['required', 'regex:/^[0-9]*$/'],
        ];
        $message = [
            'regex' => 'La clave que se esta enviado para buscar al usuario es invalida',
        ];
        $validator = Validator::make(array('id' => $id), $rul, $message);
        $errorId = $validator->errors();
        if( count($errorId) == 0 ){
            $user = User::find($id);
            if($user == null){
                return response()->json(['mensaje' => 'No se encontro ningun usuario para mostrar con esa clave'], 404);
            }else{
                return response()->json($user, 200);
            }
        }else{
            return response()->json(['error' => $errorId, 'mensaje' => 'Error en clave primaria'], 400);
        }
    }



    public function deleteUser($id){
        $rul =
        [
            'id' => ['required', 'regex:/^[0-9]*$/'],
        ];
        $message = [
            'regex' => 'La clave que se esta enviado para buscar y eliminar al usuario es invalida',
        ];
        $validator = Validator::make(array('id' => $id), $rul, $message);
        $errorId = $validator->errors();
        if( count($errorId) == 0 ){
            $user = User::find($id);
            if($user == null){
                return response()->json(['mensaje' => 'No se encontro ningun usuario para eliminar con esa clave'], 404);
            }else{
                try{
                    DB::beginTransaction();
                    $user->delete();
                    DB::commit();
                    return response()->json(['mensaje' => 'Se elimino con exito al usuario con id = '.$id],200);
                } catch(\Exception $e){
                    DB::rollback();
                    return response()->json(['mensaje' => 'Hubo error dentro del sistema, vuelve a intentarlo'],400);
                }
                return response()->json($user, 200);
            }
        }else{
            return response()->json(['error' => $errorId, 'mensaje' => 'Error en clave primaria'], 400);
        }
    }



}
