<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;


class AuthController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login', 'registerUser']]);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'No autorizado'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function payload()
    {
        return response()->json(auth()->payload());
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
        ]);
    }

    public function registerUser(Request $request){        
        try{

            $rules =
            [
                'name' => ['required','string', 'max:255', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ]*)*)+$/'],
                'user_name' => ['required','string', 'max:255', 'unique:users'],
                'phone' => ['required','string', 'max:255', 'regex:/^[0-9]*$/'],
                'birth_date' => ['required','string', 'max:255', 'regex:/^([0-2][1-9]|(3)[0-1])(\/)(((0)[1-9])|((1)[0-2]))(\/)\d{4}$/'],
                'password' => ['required', 'string', 'min:8'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ];
            $validator = Validator::make($request->all(), $rules);
            $errors = $validator->errors();
            //error_log(count($errors));            
            if(count($errors) == 0){
                DB::beginTransaction();
                $user = new User();
                $user->name = $request->name;
                $user->user_name = $request->user_name;
                $user->phone = $request->phone;
                $user->birth_date = $request->birth_date;
                $user->password = bcrypt($request->password);
                $user->email = $request->email;
                $user->save();
                DB::commit();
                return response()->json(['mensaje' => 'Se agrego con exito el nuevo usuario', $user],201);
            }else{
                return response()->json(['error' => $errors, 'mensaje' => 'No se pudo ingresar debido a errores en campos'], 400);
            }
        } catch(\Exception $e){
            DB::rollback();
            return response()->json(['mensaje' => 'Hubo error dentro del sistema, vuelve a intentarlo'],400);
        }              

    }



}
