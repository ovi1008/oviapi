<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function addProduct(Request $request){        
        try{

            $rules =
            [
                'nombre' => ['required','string', 'max:255', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ]*)*)+$/'],
                'sku' => ['nullable','string', 'min:8', 'max:12', 'unique:products'],
                'cantidad' => ['required','integer', 'numeric', 'max:255', 'regex:/^[0-9]*$/'],
                'precio' => ['required', 'regex:/^\d{1,10}+(,\d{3})*(\.\d{1,2})?$/', 'max:13', 'min:3'],
                'descripcion' => ['nullable', 'string', 'max:255'],
                'imagen' => ['nullable', 'mimes:png,jpg,jpeg'],
            ];
            $validator = Validator::make($request->all(), $rules);
            $errors = $validator->errors();            
            if(count($errors) == 0){
                DB::beginTransaction();
                $product = new Product();
                $product->sku = strtoupper($request->sku);
                $product->nombre = strtoupper($request->nombre);
                $product->cantidad = (int)$request->cantidad;
                $product->precio = floatval($request->precio);
                $product->descripcion = $request->descripcion;
                if($request->imagen != null){
                    $product->imagen = $request->imagen->store('');
                }else{
                    $product->imagen = null;                    
                }                
                $product->save();
                DB::commit();
                return response()->json(['mensaje' => 'Se agrego con exito el nuevo producto', $product],201);
            }else{
                return response()->json(['error' => $errors, 'mensaje' => 'No se pudo ingresar debido a errores en campos'], 400);
            }
        } catch(\Exception $e){
            DB::rollback();
            return response()->json(['mensaje' => 'Hubo error dentro del sistema, vuelve a intentarlo'],400);
        }              

    }

    public function updateProduct($id, Request $request){
        $rul =
        [
            'id' => ['required', 'regex:/^[0-9]*$/'],
        ];
        $message = [
            'regex' => 'La clave que se esta enviado para editar y buscar al producto es invalida',
        ];
        $validator = Validator::make(array('id' => $id), $rul, $message);
        $errorId = $validator->errors();
        if( count($errorId) == 0 ){
            $product = Product::find($id);
            if($product == null){
                return response()->json(['mensaje' => 'No se encontro ningun producto para actualizar con esa clave'], 404);
            }
        }else{
            return response()->json(['error' => $errorId, 'mensaje' => 'Error en clave primaria'], 400);
        }        
        try{

            $rules =
            [
                'nombre' => ['required','string', 'max:255', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ]*)*)+$/'],
                'sku' => 'nullable','string', 'min:8', 'max:12', 'unique:products, sku'.$id,
                'cantidad' => ['required','integer', 'numeric', 'max:255', 'regex:/^[0-9]*$/'],
                'precio' => ['required', 'regex:/^\d{1,10}+(,\d{3})*(\.\d{1,2})?$/', 'max:13', 'min:3'],
                'descripcion' => ['nullable', 'string', 'max:255'],
                'imagen' => ['nullable', 'mimes:png,jpg,jpeg'],
            ];
            $validator = Validator::make($request->all(), $rules);
            $errors = $validator->errors();            
            if(count($errors) == 0){
                DB::beginTransaction();
                $product->sku = strtoupper($request->sku);
                $product->nombre = strtoupper($request->nombre);
                $product->cantidad = (int)$request->cantidad;
                $product->precio = floatval($request->precio);
                $product->descripcion = $request->descripcion;
                if($request->imagen != null){
                    Storage::delete($product->imagen);
                    $product->imagen = $request->imagen->store('');
                }else{
                    $product->imagen = null;                    
                }                
                $product->save();
                DB::commit();
                return response()->json(['mensaje' => 'Se edito con exito el nuevo producto', $product],201);
            }else{
                return response()->json(['error' => $errors, 'mensaje' => 'No se pudo editar debido a errores en campos'], 400);
            }
        } catch(\Exception $e){
            DB::rollback();
            return response()->json(['mensaje' => 'Hubo error dentro del sistema, vuelve a intentarlo'],400);
        }              

    }

    public function getAllProduct(){
        $products = Product::paginate(3);
        if($products == null){
            return response()->json(['mensaje' => 'No se encuentra ningun producto'], 404);
        }else{
            return response()->json($products, 200);
        }
    }

    public function getProduct($id){
        $rul =
        [
            'id' => ['required', 'regex:/^[0-9]*$/'],
        ];
        $message = [
            'regex' => 'La clave que se esta enviado para buscar al producto es invalida',
        ];
        $validator = Validator::make(array('id' => $id), $rul, $message);
        $errorId = $validator->errors();
        if( count($errorId) == 0 ){
            $product = Product::find($id);
            if($product == null){
                return response()->json(['mensaje' => 'No se encontro ningun producto para mostrar con esa clave'], 404);
            }else{
                return response()->json($product, 200);
            }
        }else{
            return response()->json(['error' => $errorId, 'mensaje' => 'Error en clave primaria'], 400);
        }
    }

    public function searchProduct($value){
        $product = Product::where('nombre','like', '%'.strtoupper($value).'%')->orWhere('sku','like', '%'.strtoupper($value).'%')->paginate(3);
        if($product == null){
            return response()->json(['mensaje' => 'No se encontro ningun producto para mostrar con esa clave'], 404);
        }else{
            return response()->json($product, 200);
        }
    }

    public function deleteProduct($id){
        $rul =
        [
            'id' => ['required', 'regex:/^[0-9]*$/'],
        ];
        $message = [
            'regex' => 'La clave que se esta enviado para buscar y eliminar al producto es invalida',
        ];
        $validator = Validator::make(array('id' => $id), $rul, $message);
        $errorId = $validator->errors();
        if( count($errorId) == 0 ){
            $product = Product::find($id);
            if($product == null){
                return response()->json(['mensaje' => 'No se encontro ningun producto para eliminar con esa clave'], 404);
            }else{
                try{
                    DB::beginTransaction();
                    $product->delete();
                    DB::commit();
                    return response()->json(['mensaje' => 'Se elimino con exito el producto con id = '.$id],200);
                } catch(\Exception $e){
                    DB::rollback();
                    return response()->json(['mensaje' => 'Hubo error dentro del sistema, vuelve a intentarlo'],400);
                }
                return response()->json($product, 200);
            }
        }else{
            return response()->json(['error' => $errorId, 'mensaje' => 'Error en clave primaria'], 400);
        }
    }


}
