<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'sku', 'nombre', 'cantidad', 'precio', 'descripcion', 'imagen'
    ];
}
