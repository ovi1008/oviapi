<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'email'     => 'jorge.ovi10@gmail.com',
            'name'     => 'Jorge Mejia',
            'user_name' => 'ovi10',
            'phone' => '77777777',
            'birth_date' => '17-05-1994',
            'password' => bcrypt('admin1234')
            ]);
    }
}
