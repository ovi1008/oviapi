<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Product::create([
            'nombre'     => 'Croquetas Toffee',
            'cantidad'     => (int)'3',
            'precio' => floatval('3.76'),
            ]);
    }
}
