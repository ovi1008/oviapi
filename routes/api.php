<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('registerUser', 'AuthController@registerUser');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group([
    'prefix' => 'user',
], function () {
    Route::post('add', 'UserController@addUser');
    Route::put('update/{id}', 'UserController@updateUser');
    Route::get('show/{id}', 'UserController@getUser');
    Route::get('all', 'UserController@getAllUser');
    Route::delete('delete/{id}', 'UserController@deleteUser');
});

Route::group([
    'prefix' => 'products',
], function () {
    Route::post('add', 'ProductController@addProduct');
    Route::put('update/{id}', 'ProductController@updateProduct');
    Route::get('show/{id}', 'ProductController@getProduct');
    Route::get('all', 'ProductController@getAllProduct');
    Route::delete('delete/{id}', 'ProductController@deleteProduct');
    Route::get('search/{value}', 'ProductController@searchProduct');
});